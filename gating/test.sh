#!/bin/sh -eux
# Test to verify that FlexiBLAS swaps BLAS libraries
# by checking if OpenBLAS is at least 3 times faster than Netlib BLAS

PYTHON_SCRIPT=$(cat <<EOF
import numpy as np
import time

size = 2000
a = np.random.rand(size, size)
b = np.random.rand(size, size)

start_time = time.time()
np.dot(a, b)
end_time = time.time()

print(end_time - start_time)
EOF
)

echo "Testing with NETLIB"
netlib_time=$(FLEXIBLAS="NETLIB" python3 -c "$PYTHON_SCRIPT")
echo "Time taken with NETLIB: $netlib_time seconds"
echo "-----------------------------------"

echo "Testing with OPENBLAS-OPENMP"
openblas_time=$(FLEXIBLAS="OPENBLAS-OPENMP" python3 -c "$PYTHON_SCRIPT")
echo "Time taken with OPENBLAS-OPENMP: $openblas_time seconds"
echo "-----------------------------------"

# Verify that OpenBLAS is at least 3 times faster than Netlib BLAS
if (( $(echo "$netlib_time >= 3 * $openblas_time" | bc -l) )); then
    echo "Test passed: OpenBLAS is at least 3 times faster than Netlib BLAS."
    exit 0
else
    echo "Test failed: OpenBLAS is not at least 3 times faster than Netlib BLAS. Flexiblas might not have swapped the libraries."
    exit 1
fi

